package com.heraizen.cj.ipl.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Updates;

import org.bson.BsonNull;
import org.bson.conversions.Bson;

import static com.mongodb.client.model.Aggregates.group;
import static com.mongodb.client.model.Aggregates.project;
import static com.mongodb.client.model.Accumulators.*;

import com.heraizen.cj.ipl.domain.Player;
import com.heraizen.cj.ipl.domain.Teams;
import com.heraizen.cj.ipl.dto.PlayerDTO;
import com.heraizen.cj.ipl.dto.RoleCountDTO;
import com.heraizen.cj.ipl.dto.TeamAmountDTO;
import com.heraizen.cj.ipl.dto.TeamDTO;
import com.heraizen.cj.ipl.dto.TeamLableDTO;
import com.heraizen.cj.ipl.util.ConnectionUtil;
import static com.mongodb.client.model.Sorts.*;
import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Projections.*;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Aggregates.match;

public class IplMongoDriver implements IplService {
	private static transient IplService iplMongoDriverObj = null;
	List<Teams> teamList = null;
	private ConnectionUtil connectionUtil = ConnectionUtil.connUtil;

	private IplMongoDriver() {

	}

	public static IplService getInstance() {
		if (iplMongoDriverObj == null) {
			synchronized (IplMongoDriver.class) {
				if (iplMongoDriverObj == null) {
					iplMongoDriverObj = new IplMongoDriver();
				}
			}
		}
		return iplMongoDriverObj;
	}

	@Override
	public TeamLableDTO getTeamLabels() {
		TeamLableDTO teamLableDTO = null;
		try (MongoClient mongoClient = connectionUtil.getMongoClint()) {
			MongoDatabase database = mongoClient.getDatabase("users");
			MongoCollection<TeamLableDTO> collection = database.getCollection("ipl2020", TeamLableDTO.class);
			AggregateIterable<TeamLableDTO> result = collection
					.aggregate(Arrays.asList(group(new BsonNull(), push("labels", "$label")), project(excludeId())));
			teamLableDTO = result.first();
		}
		return teamLableDTO;
	}

	@Override
	public List<PlayerDTO> getPlayersByTeam(String teamName) {
		List<PlayerDTO> playerListDto = new ArrayList<PlayerDTO>();
		try (MongoClient mongoClient = connectionUtil.getMongoClint()) {
			MongoDatabase database = mongoClient.getDatabase("users");
			MongoCollection<PlayerDTO> collection = database.getCollection("ipl2020", PlayerDTO.class);
			AggregateIterable<PlayerDTO> result = collection.aggregate(Arrays.asList(match(eq("label", teamName)),
					unwind("$players"), project(fields(excludeId(), computed("name", "$players.name"),
							computed("role", "$players.role"), computed("price", "$players.price")))));
			for (PlayerDTO p : result) {
				playerListDto.add(p);
			}
		}
		return playerListDto;
	}

	@Override
	public List<RoleCountDTO> getRoleCountByteam(String teamName) {
		List<RoleCountDTO> roleCountList = new ArrayList<RoleCountDTO>();
		try (MongoClient mongoClient = connectionUtil.getMongoClint()) {
			MongoDatabase database = mongoClient.getDatabase("users");
			MongoCollection<RoleCountDTO> collection = database.getCollection("ipl2020", RoleCountDTO.class);
			AggregateIterable<RoleCountDTO> result = collection.aggregate(Arrays.asList(match(eq("label", teamName)),
					unwind("$players"), group("$players.role", sum("count", 1L)),
					project(fields(computed("role", "$_id"), include("count"), excludeId()))));
			for (RoleCountDTO p : result) {
				roleCountList.add(p);
			}
		}
		return roleCountList;
	}

	@Override
	public List<PlayerDTO> getPlayersByTeamAndRole(String teamName, String role) {
		List<PlayerDTO> playerListDto = new ArrayList<PlayerDTO>();
		try (MongoClient mongoClient = connectionUtil.getMongoClint()) {
			MongoDatabase database = mongoClient.getDatabase("users");
			MongoCollection<PlayerDTO> collection = database.getCollection("ipl2020", PlayerDTO.class);
			AggregateIterable<PlayerDTO> result = collection.aggregate(
					Arrays.asList(match(eq("label", teamName)), unwind("$players"), match(eq("players.role", role)),
							project(fields(excludeId(), computed("name", "$players.name"),
									computed("role", "$players.role"), computed("price", "$players.price")))));
			for (PlayerDTO p : result) {
				playerListDto.add(p);
			}
		}
		return playerListDto;
	}

	@Override
	public List<TeamDTO> getTeamDetails() {
		List<TeamDTO> teamDtoList = new ArrayList<TeamDTO>();
		try (MongoClient mongoClient = connectionUtil.getMongoClint()) {
			MongoDatabase database = mongoClient.getDatabase("users");
			MongoCollection<TeamDTO> collection = database.getCollection("ipl2020", TeamDTO.class);
			AggregateIterable<TeamDTO> result = collection
					.aggregate(Arrays.asList(project(fields(excludeId(), exclude("players")))));
			for (TeamDTO t : result) {
				teamDtoList.add(t);
			}
		}
		return teamDtoList;
	}

	@Override
	public List<TeamAmountDTO> getAmountSpentByTeam(String label) {
		List<TeamAmountDTO> teamAmountDtoList = new ArrayList<TeamAmountDTO>();
		try (MongoClient mongoClient = connectionUtil.getMongoClint()) {
			MongoDatabase database = mongoClient.getDatabase("users");
			MongoCollection<TeamAmountDTO> collection = database.getCollection("ipl2020", TeamAmountDTO.class);
			AggregateIterable<TeamAmountDTO> result = collection.aggregate(Arrays.asList(match(eq("label", "RCB")),
					unwind("$players"), group("$players.role", sum("amount", "$players.price")),
					project(fields(excludeId(), computed("label", "$_id"), include("amount")))));
			for (TeamAmountDTO t : result) {
				teamAmountDtoList.add(t);
			}
		}
		return teamAmountDtoList;
	}

	@Override
	public List<PlayerDTO> getPlayersSortByPrice() {
		List<PlayerDTO> playerListDto = new ArrayList<PlayerDTO>();
		try (MongoClient mongoClient = connectionUtil.getMongoClint()) {
			MongoDatabase database = mongoClient.getDatabase("users");
			MongoCollection<PlayerDTO> collection = database.getCollection("ipl2020", PlayerDTO.class);
			AggregateIterable<PlayerDTO> result = collection.aggregate(Arrays.asList(unwind("$players"),
					sort(descending("players.price")), project(fields(excludeId(), computed("name", "$players.name"),
							computed("role", "$players.role"), computed("price", "$players.price")))));
			for (PlayerDTO p : result) {
				playerListDto.add(p);
			}
		}
		return playerListDto;
	}

	@Override
	public Map<String, PlayerDTO> getMaxPaidPlayerForEachRole() {
		Map<String, PlayerDTO> playersList = new HashMap<>();
		try (MongoClient mongoClient = connectionUtil.getMongoClint()) {
			MongoDatabase database = mongoClient.getDatabase("users");
			MongoCollection<PlayerDTO> collection = database.getCollection("ipl2020", PlayerDTO.class);
			AggregateIterable<PlayerDTO> result = collection.aggregate(Arrays.asList(unwind("$players"),
					match(eq("players.role", "Batsman")), sort(descending("players.price")),
					project(fields(excludeId(), computed("name", "$players.name"), computed("role", "$players.role"),
							computed("price", "$players.price")))));
			playersList.put("Batsman", result.first());
			result = collection.aggregate(Arrays.asList(unwind("$players"), match(eq("players.role", "Bowler")),
					sort(descending("players.price")), project(fields(excludeId(), computed("name", "$players.name"),
							computed("role", "$players.role"), computed("price", "$players.price")))));
			playersList.put("Bowler", result.first());
			result = collection.aggregate(Arrays.asList(unwind("$players"), match(eq("players.role", "All-Rounder")),
					sort(descending("players.price")), project(fields(excludeId(), computed("name", "$players.name"),
							computed("role", "$players.role"), computed("price", "$players.price")))));
			playersList.put("All-Rounder", result.first());
			result = collection.aggregate(Arrays.asList(unwind("$players"), match(eq("players.role", "Wicket Keeper")),
					sort(descending("players.price")), project(fields(excludeId(), computed("name", "$players.name"),
							computed("role", "$players.role"), computed("price", "$players.price")))));
			playersList.put("Wicket Keeper", result.first());
		}
		return playersList;
	}

	@Override
	public List<TeamAmountDTO> getAmountSpentByEachTeam() {
		List<TeamAmountDTO> teamAmountDtoList = new ArrayList<TeamAmountDTO>();
		try (MongoClient mongoClient = connectionUtil.getMongoClint()) {
			MongoDatabase database = mongoClient.getDatabase("users");
			MongoCollection<TeamAmountDTO> collection = database.getCollection("ipl2020", TeamAmountDTO.class);
			AggregateIterable<TeamAmountDTO> result = collection
					.aggregate(Arrays.asList(unwind("$players"), group("$label", sum("totalAmount", "$players.price")),
							project(fields(excludeId(), computed("label", "$_id"), include("totalAmount")))));
			for (TeamAmountDTO t : result) {
				teamAmountDtoList.add(t);
			}
		}
		return teamAmountDtoList;
	}

	@Override
	public Teams addTeam(Teams team) {
		try (MongoClient mongoClient = connectionUtil.getMongoClint()) {
			MongoDatabase database = mongoClient.getDatabase("users");
			MongoCollection<Teams> collection = database.getCollection("ipl2020", Teams.class);
			collection.insertOne(team);
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return collection.aggregate(Arrays.asList(match(eq("home", team.getHome())))).first();

		}

//		{
//		    "city" : "Pune",
//		    "coach" : "Mahela Jaywardene",
//		    "home" : "Pune",
//		    "name" : "Pune warriers",
//		    "label" : "PW",
//		    "players" : [ 
//		        {
//		            "name" : "Pravas",
//		            "price" : 2000000.0,
//		            "role" : "Batsman"
//		        }, 
//		        {
//		            "name" : "Badal",
//		            "price" : 8000000.0,
//		            "role" : "All-Rounder"
//		        }
//		    ]
//		}
	}

	@Override
	public Teams addPlayer(String label, Player player) {
		try (MongoClient mongoClient = connectionUtil.getMongoClint()) {
			MongoDatabase database = mongoClient.getDatabase("users");
			MongoCollection<Teams> collection = database.getCollection("ipl2020", Teams.class);
			collection.updateOne(eq("label", label), Updates.addToSet("players", player));
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return collection.find(match(eq("label", label))).first();
		}
	}

	@Override
	public Teams addPlayer(String label, List<Player> playerList) {
		try (MongoClient mongoClient = connectionUtil.getMongoClint()) {
			MongoDatabase database = mongoClient.getDatabase("users");
			MongoCollection<Teams> collection = database.getCollection("ipl2020", Teams.class);
			collection.updateOne(eq("label", label), Updates.addToSet("players", playerList));
			return collection.find(match(eq("label", label))).first();
		}
	}

	@Override
	public Teams deletePlayer(String playerName) {
		try (MongoClient mongoClient = connectionUtil.getMongoClint()) {
			MongoDatabase database = mongoClient.getDatabase("users");
			MongoCollection<Teams> collection = database.getCollection("ipl2020", Teams.class);
			return collection.findOneAndDelete(match(eq("players.name", playerName)));

		}
	}

	@Override
	public Teams updateTeam(Teams team) {
		try (MongoClient mongoClient = connectionUtil.getMongoClint()) {
			MongoDatabase database = mongoClient.getDatabase("users");
			MongoCollection<Teams> collection = database.getCollection("ipl2020", Teams.class);
			return collection.findOneAndUpdate(match(eq("label", team.getLabel())), (Bson) team);

		}
	}

}
