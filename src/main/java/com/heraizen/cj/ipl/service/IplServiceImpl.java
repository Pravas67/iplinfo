package com.heraizen.cj.ipl.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import com.heraizen.cj.ipl.domain.Player;
import com.heraizen.cj.ipl.domain.Teams;
import com.heraizen.cj.ipl.dto.PlayerDTO;
import com.heraizen.cj.ipl.dto.RoleCountDTO;
import com.heraizen.cj.ipl.dto.TeamAmountDTO;
import com.heraizen.cj.ipl.dto.TeamDTO;
import com.heraizen.cj.ipl.dto.TeamLableDTO;
import com.heraizen.cj.ipl.util.JsonReader;

public class IplServiceImpl implements IplService {
	private static transient IplService iplServiceObj = null;
	List<Teams> teamList = null;
	private static final String FILE_NAME = "iplinfo.json";

	private IplServiceImpl() {
		teamList = JsonReader.readData(FILE_NAME);
	}

	public static IplService getInstance() {
		System.out.println("dhfvdshfgdshf");
		if (iplServiceObj == null) {
			synchronized (IplServiceImpl.class) {
				if (iplServiceObj == null) {
					iplServiceObj = new IplServiceImpl();
				}
			}
		}
		return iplServiceObj;
	}

	@Override
	public TeamLableDTO getTeamLabels() {
		List<String> labels = teamList.stream().map(Teams::getLabel).collect(Collectors.toList());
		return TeamLableDTO.builder().labels(labels).build();
	}

	@Override
	public List<PlayerDTO> getPlayersByTeam(String teamName) {
		List<PlayerDTO> playerByLabel = new ArrayList<>();
		Optional<Teams> playerList = teamList.stream().filter(t -> t.getLabel().equalsIgnoreCase(teamName)).findAny();
		if (playerList.isPresent()) {
			
		}
		return playerByLabel;
	}

	@Override
	public List<RoleCountDTO> getRoleCountByteam(String teamName) {
		List<PlayerDTO> playerByLabel = new ArrayList<>();
		teamList.forEach(t -> {
			if (t.getLabel().equalsIgnoreCase(teamName)) {
				t.getPlayers().forEach(p -> {
					PlayerDTO playerDTO = PlayerDTO.builder().name(p.getName()).price(p.getPrice()).role(p.getRole())
							.build();
					playerByLabel.add(playerDTO);
				});
			}
		});
		List<RoleCountDTO> roleCountDTO = new ArrayList<RoleCountDTO>();
		if (playerByLabel.size() != 0) {

			long batsman = playerByLabel.stream().filter(p -> p.getRole().equalsIgnoreCase("Batsman")).count();
			long wicketKeeper = playerByLabel.stream().filter(p -> p.getRole().equalsIgnoreCase("Wicket Keeper"))
					.count();
			long allRounder = playerByLabel.stream().filter(p -> p.getRole().equalsIgnoreCase("All-Rounder")).count();
			long bowler = playerByLabel.stream().filter(p -> p.getRole().equalsIgnoreCase("Bowler")).count();

			roleCountDTO.add(RoleCountDTO.builder().role("All-Rounder").count(allRounder).build());
			roleCountDTO.add(RoleCountDTO.builder().role("Batsman").count(batsman).build());
			roleCountDTO.add(RoleCountDTO.builder().role("Bowler").count(bowler).build());
			roleCountDTO.add(RoleCountDTO.builder().role("Wicket Keeper").count(wicketKeeper).build());
		}
		return roleCountDTO;
	}

	@Override
	public List<PlayerDTO> getPlayersByTeamAndRole(String teamName, String role) {
		List<List<Player>> listPlayers = teamList.stream().filter(t -> t.getLabel().equalsIgnoreCase(teamName))
				.map(Teams::getPlayers).collect(Collectors.toList());
		List<PlayerDTO> allPlayers = new ArrayList<PlayerDTO>();
		for (List<Player> players : listPlayers) {
			for (Player player : players) {
				if (player.getRole().equalsIgnoreCase(role)) {
					allPlayers.add(PlayerDTO.builder().name(player.getName()).price(player.getPrice())
							.role(player.getRole()).build());
				}
			}
		}

		return allPlayers;
	}

	@Override
	public List<TeamDTO> getTeamDetails() {
		List<TeamDTO> teamDetails = new ArrayList<TeamDTO>();
		teamList.forEach(team -> {
			teamDetails.add(TeamDTO.builder().name(team.getName()).coach(team.getCoach()).home(team.getHome())
					.label(team.getLabel()).build());
		});
		return teamDetails;
	}

	@Override
	public List<TeamAmountDTO> getAmountSpentByTeam(String label) {
		List<List<Player>> listPlayers = teamList.stream().filter(t -> t.getLabel().equalsIgnoreCase(label))
				.map(Teams::getPlayers).collect(Collectors.toList());
		TeamAmountDTO teamAmountDTO = null;
		List<TeamAmountDTO> teamAmountDtoList = new ArrayList<>();
		for (List<Player> players : listPlayers) {
			double amount = players.stream().mapToDouble(p -> p.getPrice()).sum();
			teamAmountDTO = TeamAmountDTO.builder().label(label).amount(amount).build();
			teamAmountDtoList.add(teamAmountDTO);
		}
		return teamAmountDtoList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PlayerDTO> getPlayersSortByPrice() {
		List<PlayerDTO> playerList = new ArrayList<>();

		teamList.forEach(t -> {
			t.getPlayers().forEach(p -> {
				PlayerDTO playerDTO = PlayerDTO.builder().name(p.getName()).price(p.getPrice()).role(p.getRole())
						.build();
				playerList.add(playerDTO);
			});
		});
		Collections.sort(playerList);
		return playerList;
	}

	@Override
	public Map<String, PlayerDTO> getMaxPaidPlayerForEachRole() {
		Map<String, PlayerDTO> playersList = new HashMap<>();
		List<List<Player>> listPlayers = teamList.stream().map(Teams::getPlayers).collect(Collectors.toList());
		List<Player> allPlayers = new ArrayList<>();
		for (List<Player> players : listPlayers) {
			allPlayers.addAll(players);

		}
		Player maxPaidForBatsman = allPlayers.stream().filter(p -> p.getRole().equalsIgnoreCase("Batsman"))
				.max(Comparator.comparing(Player::getPrice)).get();
		Player maxPaidForBowler = allPlayers.stream().filter(p -> p.getRole().equalsIgnoreCase("Bowler"))
				.max(Comparator.comparing(Player::getPrice)).get();
		Player maxPaidForAllRounder = allPlayers.stream().filter(p -> p.getRole().equalsIgnoreCase("All-Rounder"))
				.max(Comparator.comparing(Player::getPrice)).get();
		Player maxPaidForWicketKeeper = allPlayers.stream().filter(p -> p.getRole().equalsIgnoreCase("Wicket Keeper"))
				.max(Comparator.comparing(Player::getPrice)).get();

		playersList.put("Batsman", PlayerDTO.builder().name(maxPaidForBatsman.getName())
				.role(maxPaidForBatsman.getRole()).price(maxPaidForBatsman.getPrice()).build());
		playersList.put("Bowler", PlayerDTO.builder().name(maxPaidForBowler.getName()).role(maxPaidForBowler.getRole())
				.price(maxPaidForBowler.getPrice()).build());
		playersList.put("All-Rounder", PlayerDTO.builder().name(maxPaidForAllRounder.getName())
				.role(maxPaidForAllRounder.getRole()).price(maxPaidForAllRounder.getPrice()).build());
		playersList.put("Wicket Keeper", PlayerDTO.builder().name(maxPaidForWicketKeeper.getName())
				.role(maxPaidForWicketKeeper.getRole()).price(maxPaidForWicketKeeper.getPrice()).build());

		return playersList;
	}

	@Override
	public List<TeamAmountDTO> getAmountSpentByEachTeam() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Teams addTeam(Teams team) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Teams addPlayer(String label, Player player) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Teams addPlayer(String label, List<Player> playerList) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Teams deletePlayer(String playerName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Teams updateTeam(Teams team) {
		// TODO Auto-generated method stub
		return null;
	}

}
