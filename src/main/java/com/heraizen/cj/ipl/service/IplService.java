package com.heraizen.cj.ipl.service;

import java.util.List;
import java.util.Map;

import com.heraizen.cj.ipl.domain.Player;
import com.heraizen.cj.ipl.domain.Teams;
import com.heraizen.cj.ipl.dto.PlayerDTO;
import com.heraizen.cj.ipl.dto.RoleCountDTO;
import com.heraizen.cj.ipl.dto.TeamAmountDTO;
import com.heraizen.cj.ipl.dto.TeamDTO;
import com.heraizen.cj.ipl.dto.TeamLableDTO;

public interface IplService {
	TeamLableDTO getTeamLabels();

	List<PlayerDTO> getPlayersByTeam(String teamName);

	List<RoleCountDTO> getRoleCountByteam(String teamName);

	List<PlayerDTO> getPlayersByTeamAndRole(String teamName, String role);

	List<TeamDTO> getTeamDetails();

	List<TeamAmountDTO> getAmountSpentByTeam(String label);

	List<PlayerDTO> getPlayersSortByPrice();

	Map<String, PlayerDTO> getMaxPaidPlayerForEachRole();

	public List<TeamAmountDTO> getAmountSpentByEachTeam();

	public Teams addTeam(Teams team);

	public Teams addPlayer(String label, Player player);

	public Teams addPlayer(String label, List<Player> playerList);

	public Teams deletePlayer(String playerName);

	public Teams updateTeam(Teams team);
}
