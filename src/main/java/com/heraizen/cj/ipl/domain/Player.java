package com.heraizen.cj.ipl.domain;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Player {
//	private String id;
	private String name;
	private double price;
	private String role;
	
}
