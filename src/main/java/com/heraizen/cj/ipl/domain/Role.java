package com.heraizen.cj.ipl.domain;

public enum Role {
Batsman,
Bowler,
WicketKeeper,
AllRounder
}
