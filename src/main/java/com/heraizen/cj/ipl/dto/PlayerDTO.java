package com.heraizen.cj.ipl.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlayerDTO implements Comparable {
//	String id;
	String name;
	String role;
	double price;
//	String label;

	@Override
	public int compareTo(Object player) {
		double comparePrice = ((PlayerDTO) player).getPrice();
		return (int) (this.price - comparePrice);
	}
}
