package com.heraizen.cj.ipl.dto;

import java.util.List;

import com.heraizen.cj.ipl.domain.Player;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TeamDTO {
	private String city;
	private String coach;
	private String home;
	private String name;
	private String label;
//	private String id;
	private List<Player> players;
}
