package com.heraizen.cj.ipl.main;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import com.heraizen.cj.ipl.domain.Player;
import com.heraizen.cj.ipl.domain.Teams;
import com.heraizen.cj.ipl.dto.PlayerDTO;
import com.heraizen.cj.ipl.dto.RoleAmountDTO;
import com.heraizen.cj.ipl.dto.RoleCountDTO;
import com.heraizen.cj.ipl.dto.TeamAmountDTO;
import com.heraizen.cj.ipl.dto.TeamDTO;
import com.heraizen.cj.ipl.dto.TeamLableDTO;
import com.heraizen.cj.ipl.service.IplMongoDriver;
import com.heraizen.cj.ipl.service.IplService;
import com.heraizen.cj.ipl.service.IplServiceImpl;

public class IplOperation {
	private static IplService iplMongoDriverObj = IplMongoDriver.getInstance();
	Scanner sc = new Scanner(System.in);

	public void start() {

		do {
			System.out.println(
					"----------------------------------------------------------------------------------------------------------------------------------------");
			System.out.println(
					"1.View Team Labels \t 2.Get Player By Labels \t3. Get Role Count By Label  \t4.Get Player by team and Role \t5 View Team Details \n 6.View Amount spent by team on each role"
							+ "\t 7.View Amount spent By Team" + "\t 8.View Max Paid Player For Each Role "
							+ " \t 9. View All Players sort by price" + " \t 10. Add team"
							+ " \t 11.Add player to the team" + " \t 12.Add players list to the team"
							+ " \t 13.Delete player by name" + " \t 14.Update team" + "\t15.Exit");
			System.out.println(
					"----------------------------------------------------------------------------------------------------------------------------------------");
			System.out.println("Enter the choice");
			int choice = this.getUserChoice();
			switch (choice) {
			case 1:
				this.viewTeamLabels();
				break;
			case 2:
				this.getPlayersByLabel();
				break;
			case 3:
				this.getRoleCountBylabel();
				break;
			case 4:
				this.getPlayerByLabelAndRole();
				break;
			case 5:
				this.viewTeamDetails();
				break;
			case 6:
				this.viewAmountSpentedByRoleAndLabel();
				break;
			case 7:
				this.viewAmountSpentedByTeam();
				break;
			case 8:
				this.viewMaxPaidPlayerForEachRole();
				break;
			case 9:
				this.viewAllPlayers();
				break;
			case 10:
				this.addTeam();
				break;
			case 11:
				this.addPlayerToTeam();
				break;
			case 12:
				this.addPlayersToTeam();
				break;
			case 13:
				this.deletePlayer();
				break;
			case 14:
				this.updateTeam();
				break;
			case 15:
				sc.close();
				System.out.println("Thank you........");
				System.exit(0);
				break;
			}
		} while (true);

	}

	private void updateTeam() {
		List<Player> players = new ArrayList<Player>();
		players.add(Player.builder().name("Pravas").role("Role").price(500000).build());
		Teams team = Teams.builder().city("pune").home("Pune").coach("Dravid").label("PW").name("Pune warriers")
				.players(players).build();
		Teams teams = iplMongoDriverObj.updateTeam(team);

	}

	private void deletePlayer() {
		System.out.println("Enter player name to delete");
		Teams team = iplMongoDriverObj.deletePlayer(sc.nextLine());

	}

	private void addPlayerToTeam() {
		System.out.println("Enter team label");
		List<Player> players = new ArrayList<Player>();
		players.add(Player.builder().name("Subash").role("Batsman").price(500000).build());
		players.add(Player.builder().name("manu").role("All-Rounder").price(500000).build());
		Teams team = iplMongoDriverObj.addPlayer(sc.nextLine(), players);

	}

	private void addPlayersToTeam() {
		System.out.println("Enter team label");
		Player player = Player.builder().name("Pusparaj").role("All-Rounder").price(500000).build();
		Teams team = iplMongoDriverObj.addPlayer(sc.nextLine(), player);

	}

	private void addTeam() {
		List<Player> players = new ArrayList<Player>();
		players.add(Player.builder().name("Pravas").role("Batsman").price(500000).build());
		players.add(Player.builder().name("Badal").role("Batsman").price(500000).build());
		Teams team = Teams.builder().city("pune").home("Pune").coach("Dravid").label("PW").name("Pune warriers")
				.players(players).build();
		Teams teams = iplMongoDriverObj.addTeam(team);

	}

	private void viewAmountSpentedByTeam() {
		System.out.println("Enter the label or team name");
		String teamName = sc.nextLine();
		List<TeamAmountDTO> teamAmountDtoList = iplMongoDriverObj.getAmountSpentByTeam(teamName);
		System.out.println("-------------------------------------------");
		System.out.format("%-20s%-20s-\n", "Team Name", "Amount");
		System.out.println("-------------------------------------------");
		if (teamAmountDtoList != null) {
			for (TeamAmountDTO teamAmountDto : teamAmountDtoList) {
				System.out.format("%-20s%-20s-\n", teamAmountDto.getLabel().toUpperCase(), teamAmountDto.getAmount());
			}
			System.out.println();
		} else {
			System.out.println("Team Label is not available");
		}

	}

	private void viewTeamDetails() {
		List<TeamDTO> teamDetails = iplMongoDriverObj.getTeamDetails();
		System.out.println(
				"--------------------------------------------------------------------------------------------------------------------------------------------");
		System.out.format("%-30s%-30s%-50s%-20s\n", "Team Name", "Coach Name", "Home Ground", "Team Label");
		System.out.println(
				"-------------------------------------------------------------------------------------------------------------------------------------------");
		teamDetails.forEach(team -> {
			System.out.format("%-30s%-30s%-50s%-20s\n", team.getName(), team.getCoach(), team.getHome(),
					team.getLabel());

		});
	}

	private void viewAllPlayers() {
		List<PlayerDTO> players = iplMongoDriverObj.getPlayersSortByPrice();
		this.viewPlayers(players);
	}

	private void viewMaxPaidPlayerForEachRole() {
		Map<String, PlayerDTO> playersList = iplMongoDriverObj.getMaxPaidPlayerForEachRole();
		System.out
				.println("-------------------------------------------------------------------------------------------");
		System.out.format("%-30s%-20s%-20s\n", "Player Name", "Role", "Price");
		System.out
				.println("-------------------------------------------------------------------------------------------");

		for (String role : playersList.keySet()) {
			PlayerDTO p = playersList.get(role);
			System.out.format("%-30s%-20s%-20s\n", p.getName(), p.getRole(), p.getPrice());
		}

		System.out.println();
	}

	private void viewAmountSpentedByRoleAndLabel() {
		System.out.println("Enter the label");
		String label = sc.nextLine();
		List<TeamAmountDTO> amountSpent = new ArrayList<>();
		amountSpent = iplMongoDriverObj.getAmountSpentByTeam(label);
		if (amountSpent.size() != 0) {

			System.out.println("-------------------------------------------");
			System.out.format("%-20s%-20s\n", "Role", "Amount");
			System.out.println("-------------------------------------------");
			amountSpent.forEach(ele -> {
				System.out.format("%-20s%-20s\n", ele.getLabel(), ele.getAmount());
			});
		} else {
			System.out.println("Team label is not available");
		}

	}

	private void getPlayerByLabelAndRole() {
		System.out.println("Enter the Label");
		String label = sc.nextLine();
		System.out.println("Enter the Role");
		String role = sc.nextLine();
		List<PlayerDTO> players = new ArrayList<>();
		players = iplMongoDriverObj.getPlayersByTeamAndRole(label, role);
		if (players.size() != 0) {
			this.viewPlayers(players);
		} else {
			System.out.println("Team label is not available");
		}
	}

	private void getRoleCountBylabel() {
		System.out.println("Enter the label or team name");
		String LabelOrTeamName = sc.nextLine();
		List<RoleCountDTO> roleCount = new ArrayList<>();
		roleCount = iplMongoDriverObj.getRoleCountByteam(LabelOrTeamName);
		if (roleCount.size() != 0) {
			System.out.println("-------------------------------------------");
			System.out.format("%-20s%-20s\n", "Role", "Number Of Players");
			System.out.println("-------------------------------------------");
			roleCount.stream().forEach(ele -> {
				System.out.format("%-20s%-20s\n", ele.getRole(), ele.getCount());
			});
		} else {
			System.out.println("Team label is not available");
		}
	}

	private void getPlayersByLabel() {
		System.out.println("Enter the label or team name");
		String LabelOrTeamName = sc.nextLine();
		List<PlayerDTO> players = new ArrayList<>();
		players = iplMongoDriverObj.getPlayersByTeam(LabelOrTeamName);
		if (players.size() != 0) {
			this.viewPlayers(players);
		} else {
			System.out.println("Team label is not available");
		}

	}

	private void viewTeamLabels() {
		TeamLableDTO teamLables = iplMongoDriverObj.getTeamLabels();
		System.out.println("Team Label");
		teamLables.getLabels().forEach(team -> {
			System.out.println(team);
		});

	}

	private void viewPlayers(List<PlayerDTO> players) {
		System.out
				.println("-------------------------------------------------------------------------------------------");
		System.out.format("%-30s%-20s%-20s\n", "Player Name", "Role", "Price");
		System.out
				.println("-------------------------------------------------------------------------------------------");
		players.stream().forEach(p -> {
			System.out.format("%-30s%-20s%-20s\n", p.getName(), p.getRole(), p.getPrice());
		});

		System.out.println();
	}

	private int getUserChoice() {
		boolean isNumber = true;
		int choice = 0;
		try {
			choice = Integer.parseInt(this.sc.nextLine());
		} catch (NumberFormatException e) {
			isNumber = false;
		}
		while (!(choice > 0 && choice < 8 || isNumber)) {
			System.out.println("Choice must be 1 to 7 only");
			try {
				choice = Integer.parseInt(this.sc.nextLine());
			} catch (NumberFormatException e) {
				isNumber = false;
			}
		}
		return choice;
	}
}
