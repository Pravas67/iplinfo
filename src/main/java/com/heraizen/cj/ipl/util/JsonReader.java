package com.heraizen.cj.ipl.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.yaml.snakeyaml.Yaml;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.heraizen.cj.ipl.domain.Teams;

public class JsonReader {
	private JsonReader() {

	}

	public static List<Teams> readData(String fileName) {
		return readTeamByYaml("ipl2020.yaml");
	}

	private static List<Teams> readTeamByJson(String FILE_NAME) {
		List<Teams> teams = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			teams = mapper.readValue(new File(FILE_NAME), new TypeReference<List<Teams>>() {
			});
		} catch (IOException e) {

			e.printStackTrace();
		}
		return teams;
	}

	private static List<Teams> readTeamByYaml(String FILE_NAME) {
		List<Teams> teamList = new ArrayList<Teams>();
		Yaml yaml = new Yaml();
		teamList = Arrays.asList(yaml.loadAs(JsonReader.class.getResourceAsStream("/ipl2020.yaml"), Teams[].class));
		return teamList;
	}
}
