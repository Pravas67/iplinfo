package com.heraizen.cj.ipl.util;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

import java.io.IOException;
import java.util.Properties;

import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;

public enum ConnectionUtil {
	connUtil;

	private Properties properties;
//	private static final String MONGO_URI = "mongo.db.uri";

	private ConnectionUtil() {
		try {
			properties = new Properties();
			properties.load(ConnectionUtil.class.getResourceAsStream("/application.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public MongoClient getMongoClint() {
		CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
				fromProviders(PojoCodecProvider.builder().automatic(true).build()));

		MongoClientSettings settings = MongoClientSettings.builder().codecRegistry(pojoCodecRegistry)
				.applyConnectionString(new ConnectionString("mongodb+srv://dbUser:dbUser@cluster0.kbfyx.mongodb.net/users?retryWrites=true&w=majority")).build();
		MongoClient client = MongoClients.create(settings);
		return client;
	}
}
